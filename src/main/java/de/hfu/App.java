package de.hfu;
import java.util.Scanner;

/**
 * Hauptklasse des Praktikumprojekts
 */

public class App {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Geben Sie eine Zeichenkette ein:");
        String inputString = scanner.nextLine();
        System.out.println(inputString.toUpperCase());
        scanner.close();

    }
}