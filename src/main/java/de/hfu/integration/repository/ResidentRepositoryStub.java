package de.hfu.integration.repository;

import de.hfu.integration.domain.Resident;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ResidentRepositoryStub implements ResidentRepository{
    public List<Resident> getResidents(){
        //String givenName, String familyName, String street, String city, Date dateOfBirth
        List<Resident> result = new ArrayList<>();
        Resident res1 = new Resident("Max", "Mustermann", "Furtwangenerstr. 12", "Furtwangen", new Date(1980-11-30));
        Resident res2 = new Resident("Anna", "Musterfrau", "Hauptstraße 1", "Berlin", new Date(1990-01-01));
        Resident res3 = new Resident("Peter", "Schmidt", "Musterstraße 2", "Hamburg", new Date(1985-05-05));
        Resident res4 = new Resident("Hans", "Müller", "Musterweg 3", "München", new Date(1975-12-24));
        Resident res5 = new Resident("Klaus", "Schulze", "Musterplatz 4", "Frankfurt", new Date(1988-06-30));
        Resident res6 = new Resident("Maria", "Meier", "Musterweg 5", "Stuttgart", new Date(1970-03-15));
        Resident res7 = new Resident("Sandra", "Schneider", "Musterstraße 6", "Düsseldorf", new Date(1995-07-20));
        Resident res8 = new Resident("Thomas", "Fischer", "Musterweg 7", "Köln", new Date(1983-11-11));
        Resident res9 = new Resident("Julia", "Schwarz", "Musterstraße 8", "Hannover", new Date(1992-07-07));
        Resident res10 = new Resident("Michael", "Wagner", "Musterweg 9", "Leipzig", new Date(1982-02-28));
        result.add(res1);
        result.add(res2);
        result.add(res3);
        result.add(res4);
        result.add(res5);
        result.add(res6);
        result.add(res7);
        result.add(res8);
        result.add(res9);
        result.add(res10);


        return result;
    }
}