package de.hfu;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import de.hfu.unit.Queue;

public class QueueTest {

    @Test
    public void testEnqueueDequeue() {
        Queue queue = new Queue(3);

        // Enqueue three elements
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);

        // Dequeue three elements
        assertEquals(1, queue.dequeue());
        assertEquals(2, queue.dequeue());
        assertEquals(3, queue.dequeue());
    }

    @Test
    public void testEnqueueOverflow() {
        Queue queue = new Queue(3);

        // Enqueue four elements (overflow)
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4); // This should overwrite the first element

        // Dequeue three elements
        assertEquals(1, queue.dequeue());
        assertEquals(2, queue.dequeue());
        assertEquals(4, queue.dequeue());
    }

    @Test
    public void testDequeueUnderflow() {
        Queue queue = new Queue(3);

        // Attempt to dequeue from an empty queue
        assertThrows(IllegalStateException.class, () -> queue.dequeue());
    }

    @Test
    public void testCircularBuffer() {
        Queue queue = new Queue(3);

        // Enqueue three elements
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);

        // Dequeue three elements
        assertEquals(1, queue.dequeue());
        assertEquals(2, queue.dequeue());
        assertEquals(3, queue.dequeue());

        // Enqueue three elements again, testing circular buffer
        queue.enqueue(4);
        queue.enqueue(5);
        queue.enqueue(6);

        // Dequeue three elements
        assertEquals(4, queue.dequeue());
        assertEquals(5, queue.dequeue());
        assertEquals(6, queue.dequeue());
    }
}
