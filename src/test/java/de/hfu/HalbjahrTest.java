package de.hfu;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;
import static de.hfu.unit.Util.*;


@DisplayName("Test der Klasse App")
public class HalbjahrTest {

    @BeforeAll
    static void initAll() {
    }


    @BeforeEach
    void init() {
    }

    @Test
    void aequalenzklassefuertrue() {
        assertTrue(istErstesHalbjahr(1));
        assertTrue(istErstesHalbjahr(6));

    }
    @Test
    void aequalenzklassefuerfalse() {

        assertFalse(istErstesHalbjahr(7)); //hier war der Fehler
        assertFalse(istErstesHalbjahr(12));


    }
    @Test
    void aequalenzklassefuerausserhalb() {

        assertThrows(IllegalArgumentException.class, () -> {
            istErstesHalbjahr(0);
            istErstesHalbjahr(13);
        }, "IllegalArgumentException was expected");

    }

    @AfterEach
    void tearDown() {
    }

    @AfterAll
    static void tearDownAll() {
    }
}